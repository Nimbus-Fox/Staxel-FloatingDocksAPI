﻿using Plukit.Base;

namespace NimbusFox.FloatingDocksAPI.Classes {
    public class DockAnimation {
        public float Max { get; } = 0.125f;
        public float Min { get; } = 0.025f;
        public float Stage { get; } = 0.00015f;
        public Vector3D OffSet { get; } = new Vector3D(0.0, 0.0, 0.0);
        public Vector3D ItemCountDisplayOffSet { get; } = new Vector3D(0, 0, 0.15);
        public float RotationSpeed { get; } = 0.0003f;

        public DockAnimation(Blob config) {
            Max = (float) config.GetDouble("max", Max);
            Min = (float) config.GetDouble("min", Min);
            Stage = (float) config.GetDouble("step", Stage);

            if (config.Contains("offset")) {
                OffSet = config.FetchBlob("offset").GetVector3D();
            }

            if (config.Contains("itemCountDisplayOffset")) {
                ItemCountDisplayOffSet = config.FetchBlob("itemCountDisplayOffset").GetVector3D();
            }

            RotationSpeed = (float) config.GetDouble("rotationSpeed", RotationSpeed);
        }
    }
}
