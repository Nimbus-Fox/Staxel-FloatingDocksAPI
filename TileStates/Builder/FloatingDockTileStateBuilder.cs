﻿using System;
using NimbusFox.FloatingDocksAPI.TileStates.EntityBuilders;
using Plukit.Base;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates;

namespace NimbusFox.FloatingDocksAPI.TileStates.Builders {
    public class FloatingDockTileStateBuilder : ITileStateBuilder, IDisposable {
        public void Dispose() { }
        public void Load() { }
        public string Kind() {
            return KindCode();
        }

        public Entity Instance(Vector3I location, Tile tile, Universe universe) {
            return FloatingDockTileStateEntityBuilder.Spawn(universe, tile, location);
        }

        public static string KindCode() {
            return "nimbusfox.floatingdocksapi.tileState.floatingdock";
        }
    }
}
