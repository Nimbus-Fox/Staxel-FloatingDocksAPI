﻿using System;
using NimbusFox.FloatingDocksAPI.DockSites;
using Plukit.Base;
using Staxel.Core;
using Staxel.Docks;
using Staxel.Logic;
using Staxel.TileStates.Docks;

namespace NimbusFox.FloatingDocksAPI.TileStates.Logic {
    public class FloatingDockTileStateEntityLogic : DockTileStateEntityLogic {
        public FloatingDockTileStateEntityLogic(Entity entity) : base(entity) { }

        public override void Update(Timestep timestep, EntityUniverseFacade universe) {
            base.Update(timestep, universe);

            _dockSites.ForEach(x => x.Update(timestep, universe));
        }

        public override Vector3F InteractCursorColour() {
            return !HasAnyDockedItems() ? base.InteractCursorColour() : Constants.InteractCursorColour;
        }

        protected override void AddSite(DockSiteConfiguration config) {
            if (config.SiteName.StartsWith(FloatingDockSite.Name, StringComparison.CurrentCultureIgnoreCase)) {
                _dockSites.Add(new FloatingDockSite(Entity, new DockSiteId(Entity.Id, _dockSites.Count), config, GetTileConfig()));
                return;
            }

            base.AddSite(config);
        }

        public override void Store() {
            base.Store();
            StorePersistenceData(Entity.Blob);
        }

        public override void Restore() {
            base.Restore();
            Restore(Entity.Blob);
        }

        public override void RestoreFromPersistedData(Blob data, EntityUniverseFacade facade) {
            base.RestoreFromPersistedData(data, facade);
            Restore(data);
        }

        public virtual void Restore(Blob data) {
        }
    }
}
