﻿using System;
using Microsoft.Xna.Framework;
using NimbusFox.FloatingDocksAPI.Classes;
using NimbusFox.FloatingDocksAPI.DockSites;
using Plukit.Base;
using Staxel;
using Staxel.Client;
using Staxel.Core;
using Staxel.Draw;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Rendering;
using Staxel.TileStates.Docks;

namespace NimbusFox.FloatingDocksAPI.TileStates.Painters {
    public class FloatingDockTileStateEntityPainter : DockTileStateEntityPainter {
        protected readonly BillboardNumberRenderer BillboardNumberRenderer = new BillboardNumberRenderer();
        private bool _up = true;
        private float _add;
        public FloatingDockTileStateEntityPainter(DockTileStateEntityBuilder builder) : base(builder) { }

        public override void Render(DeviceContext graphics, ref Matrix4F matrix, Vector3D renderOrigin, Entity entity, AvatarController avatarController, Timestep renderTimestep, RenderMode renderMode) {
            BillboardNumberRenderer.Purge();
            var stateEntityLogic = entity.DockTileStateEntityLogic;
            if (renderMode == RenderMode.Inside)
                return;
            if (stateEntityLogic.GetContainerState() == ContainerState.Closed)
                return;
            if (renderMode == RenderMode.Shadow) {
                if (!PlayerInRange(EntityPosition(renderTimestep, entity), avatarController, renderTimestep, Constants.DockedItemRenderShadowCullDistanceSquared))
                    return;
            } else if (!PlayerInRange(EntityPosition(renderTimestep, entity), avatarController, renderTimestep, Constants.DockedItemRenderCullDistanceSquared))
                return;

            var bottomCenter = stateEntityLogic.GetBottomCenter();
            foreach (var site in stateEntityLogic.GetSites()) {
                if (!site.IsEmpty() && site is FloatingDockSite floatingSite) {
                    var dockedWorldPosition = site.GetItemDockedWorldPosition(bottomCenter);
                    var origCenter = site.GetItemDockedWorldPosition(bottomCenter);
                    var vector3D1 = site.Rotation.ToVector3D();

                    dockedWorldPosition.X += floatingSite.OffSet.X;
                    dockedWorldPosition.Y += floatingSite.OffSet.Y;
                    dockedWorldPosition.Z += floatingSite.OffSet.Z;

                    origCenter.X += floatingSite.OffSet.X;
                    origCenter.Y += floatingSite.OffSet.Y;
                    origCenter.Z += floatingSite.OffSet.Z;

                    if (dockedWorldPosition.Y + _add >= origCenter.Y + floatingSite.Max) {
                        _up = false;
                    }

                    if (dockedWorldPosition.Y + _add <= origCenter.Y + floatingSite.Min) {
                        _up = true;
                    }

                    var tag = this.GetPrivateFieldValue<NameTag>("_itemTag", typeof(DockTileStateEntityPainter));

                    vector3D1.X = (DateTime.UtcNow - new DateTime(0L)).TotalMilliseconds * floatingSite.RotationSpeed % (2 * Math.PI);
                    _add = _up ? _add + floatingSite.Stage : _add - floatingSite.Stage;
                    dockedWorldPosition.Y += _add;

                    tag?.Setup(dockedWorldPosition, new Vector3D(0, 0.325, 0.25),
                        GetItemTag(floatingSite, avatarController, out var usePetalTag, out var icon), false, usePetalTag, false, icon);

                    var m = Matrix.CreateFromYawPitchRoll((float) vector3D1.X, (float) vector3D1.Y, (float) vector3D1.Z)
                        .ToMatrix4F();

                    var dockedMatrix = ItemConfiguration.DetermineDockedMatrix(
                        ref m, ref matrix, renderOrigin, floatingSite.DockedItem.Stack.Item, dockedWorldPosition);
                    ClientContext.ItemRendererManager.RenderInWorldFullSized(site.DockedItem.Stack.Item, graphics, ref dockedMatrix, site.Config.Compact, false);
                    if (renderMode == RenderMode.Normal && stateEntityLogic.RenderBillboards && site.DockedItem.Stack.Count > 1 && !site.DockedItem.NotUndockable && PlayerInRange(EntityPosition(renderTimestep, entity), avatarController, renderTimestep, Constants.DockedItemCountRenderCullDistanceSquared)) {
                        var z = site.DockedItem.Stack.Item.Configuration.ModelBoundingBox.Radius.Z + Constants.DockedItemCountBillboardRadius;
                        var x = site.DockedItem.Stack.Item.Configuration.ModelBoundingBox.Radius.X + Constants.DockedItemCountBillboardRadius;
                        if (site.Config.Compact) {
                            z *= Constants.CompactDrawableScalingFactor;
                            x *= Constants.CompactDrawableScalingFactor;
                        }

                        var offset = new Vector3D(x + floatingSite.ItemCountDisplayOffSet.X,
                            Constants.DockedItemCountBillboardRadius + floatingSite.ItemCountDisplayOffSet.Y,
                            z + floatingSite.ItemCountDisplayOffSet.Z);
                        BillboardNumberRenderer.DrawInteger(site.DockedItem.Stack.Count, dockedWorldPosition, offset);
                    }
                }
            }
            if (renderMode != RenderMode.Normal)
                return;
            BillboardNumberRenderer.Draw(graphics, renderOrigin, ref matrix);
        }

        public override string GetItemTag(DockSite site, AvatarController avatar, out bool usePetalTag, out string icon) {
            icon = "";
            usePetalTag = false;
            if (avatar.LookingAtDockedItem().IsNull()) {
                return "";
            }

            return avatar.LookingAtDockedItem().Item.GetItemTranslation(ClientContext.LanguageDatabase);
        }
    }
}
