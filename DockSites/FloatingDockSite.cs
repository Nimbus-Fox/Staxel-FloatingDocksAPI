﻿using System.Linq;
using NimbusFox.FloatingDocksAPI.Components.Tiles;
using Plukit.Base;
using Staxel.Docks;
using Staxel.Logic;
using Staxel.Tiles;
using Staxel.TileStates.Docks;

namespace NimbusFox.FloatingDocksAPI.DockSites {
    public class FloatingDockSite : DockSite {
        public float Max { get; } = 0.125f;
        public float Min { get; } = 0.025f;
        public float Stage { get; } = 0.00015f;
        public Vector3D OffSet { get; } = new Vector3D(0.0, 0.0, 0.0);
        public Vector3D ItemCountDisplayOffSet { get; } = new Vector3D(0, 0, 0.15);
        public float RotationSpeed { get; } = 0.0003f;

        public FloatingDockSite(Entity entity, DockSiteId id, DockSiteConfiguration dockSiteConfig, TileConfiguration parent) : base(entity,
            id, dockSiteConfig) {
            var component = parent.Components.Select<DockAnimationTileComponent>().FirstOrDefault();

            if (component != default(DockAnimationTileComponent)) {
                if (component.DockAnimations.Any(x => x.Key == dockSiteConfig.SiteName)) {
                    var config = component.DockAnimations.First(x => x.Key == dockSiteConfig.SiteName).Value;

                    Max = config.Max;
                    Min = config.Min;
                    Stage = config.Stage;
                    OffSet = config.OffSet;
                    ItemCountDisplayOffSet = config.ItemCountDisplayOffSet;
                    RotationSpeed = config.RotationSpeed;
                }
            }
        }

        public static string Name => "floatingDock";

        public void SetLocation(Vector3F location) {
            Location = location;
        }

        public void SetLocation(Vector3D location) {
            SetLocation(location.ToVector3F());
        }

        public void SetRotation(Vector3F rotation) {
            Rotation = rotation;
        }

        public void SetRotation(Vector3D rotation) {
            SetRotation(rotation.ToVector3F());
        }
    }
}
