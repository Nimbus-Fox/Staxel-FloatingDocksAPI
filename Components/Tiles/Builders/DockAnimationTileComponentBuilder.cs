﻿using Plukit.Base;
using Staxel.Tiles;

namespace NimbusFox.FloatingDocksAPI.Components.Tiles.Builders {
    public class DockAnimationTileComponentBuilder : ITileComponentBuilder {
        public string Kind() {
            return "dockAnimations";
        }

        public object Instance(TileConfiguration tile, Blob config) {
            return new DockAnimationTileComponent(config);
        }
    }
}
