﻿using System.Collections.Generic;
using NimbusFox.FloatingDocksAPI.Classes;
using Plukit.Base;

namespace NimbusFox.FloatingDocksAPI.Components.Tiles {
    public class DockAnimationTileComponent {
        public IReadOnlyDictionary<string, DockAnimation> DockAnimations { get; }

        public DockAnimationTileComponent(Blob config) {
            var animations = new Dictionary<string, DockAnimation>();

            foreach (var key in config.KeyValueIteratable.Keys) {
                animations.Add(key, new DockAnimation(config.FetchBlob(key)));
            }

            DockAnimations = animations;
        }
    }
}
